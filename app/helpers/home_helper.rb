module HomeHelper
    def replace_text(text)
        if text.length > 350
            return text[0..350]+"..."
        else
            return text
        end
    end
end
