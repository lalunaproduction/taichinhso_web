class Admin::UsersController < Admin::ApplicationController
  
  def new
    @user = User.new
  end

  def create
    
    if params["email"].nil?
      @user = User.new(user_params)
      @user.created_by = "Admin"
      @user.updated_by = "Admin"
    else
      @user = User.new
      @user.email = params["email"]
      @user.password = params["password"]
      if @user.role_id.nil?
        @user.role_id = 2
      end
      @user.created_by = "system"
      @user.updated_by = "system"
    end
    
    #generate Token
    
    #o = [('a'..'z'), ('A'..'Z'), (0..9)].map(&:to_a).flatten
    #@user.token = (0...32).map { o[rand(o.length)] }.join
    
    #@user.provider = "Website"
    @user.delete_flag = 0
    
    #TODO: 
    #kiem tra user da ton tai hay chua (name/email/sdt)
    if User.isUserValid?(@user.name)
      if @user.save
          flash[:success] = 'Tạo mới thành công'
          redirect_to admin_users_path
      else
        render 'new'
      end
    else
      render 'new'
    end
  end

  def edit
    @user = User.find(params[:id])
  end

  def update
   
  end

  def index
    @users = User.where(delete_flag: 0).all
  end

  def show
  end

  def destroy
  end
  
  private
    def user_params
      params.require(:user).permit(:name, :role_id,:email, :password)
    end
end
