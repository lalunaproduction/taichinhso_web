class Admin::PostsController < Admin::ApplicationController
  def new
    @post = Post.new
  end

  def create
    @post = Post.new(post_params)
    @post.view = 0
    @post.created_by = "Admin"
    @post.updated_by = "Admin"
    @post.delete_flag = 0
    
    if @post.save
      redirect_to admin_posts_path
    else
      render :new
    end
    
  end

  def edit
  end

  def update
  end

  def index
    @posts = Post.where(delete_flag: 0).order('created_at DESC')
  end

  def show
  end

  def destroy
  end
  
  private
    def post_params
      params.require(:post).permit(:title, :description, :content, :image, :category_id)
    end
end
