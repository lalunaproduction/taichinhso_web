class Admin::CategoriesController < Admin::ApplicationController
  #before_filter :verify_logged
  
  def new
    @category = Category.new
  end

  def create
    @category = Category.new(category_params)
    @category.created_by = "Admin"
    @category.updated_by = "Admin"
    @category.delete_flag = 0
    if @category.parent_id.nil?
      @category.parent_id = 0
    end
    
    if @category.save
      flash[:success] = 'Tạo mới thành công'
      redirect_to admin_categories_path
    else
      render 'new'
    end
  end

  def index
    @categories = Category.where(delete_flag: 0).all
  end

  def show
  end

  def edit
  end

  def update
  end

  def destroy
  end
  
  private
    def category_params
      params.require(:category).permit(:name, :description, :created_by, :updated_by, :delete_flag, :parent_id)
    end
end
