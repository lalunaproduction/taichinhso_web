class Admin::CoinsController < Admin::ApplicationController
  def new
    @coin = Coin.new
  end

  def create
    @coin = Coin.new(coin_params)
    @coin.created_by = "Admin"
    @coin.updated_by = "Admin"
    @coin.delete_flag = 0
    if @coin.save
      redirect_to admin_coins_path
    else
      render :new
    end
    
  end

  def edit
  end

  def update
  end

  def index
    @coins = Coin.where(delete_flag: 0).order('created_at DESC')
  end

  def show
  end

  def destroy
  end
  
  private
    def coin_params
      params.require(:coin).permit(:name, :image, :website, :ico_from, :ico_to, :industry, :slug)
    end
end
