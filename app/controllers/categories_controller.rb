class CategoriesController < ApplicationController
  def index
    @firstCoin = Coin.order("created_at DESC").offset(0).first
    @id = params[:id].downcase
    
    unless @id.nil?
      @name = getCategoryName(@id)
    else
      redirect_to root_path
      return
    end
    
    @id = Category.find_by(name: @name).id
    @firstPost = Post.where(category_id: @id).order("created_at DESC").offset(0).first
    @secondPosts = Post.where(category_id: @id).order("created_at DESC").offset(1).first(3)
    @listPost = Post.where(category_id: @id).order("created_at DESC").offset(4).all
  end

  def show
    if params["format"] == "html"
      @cat_id = params[:cat_id].downcase
      unless @cat_id.nil?
        @catName = getCategoryName(@cat_id)
        @catId = Category.find_by(name: @catName).id
        
        @listTop3PostInCat = Post.where(category_id: @catId).order("created_at DESC").offset(0).first(3)
        
        @postId = params[:id]
        
        unless @postId.nil?
          if Post.friendly.exists_by_friendly_id?(@postId)
            @post = Post.friendly.find(@postId)
          else
            redirect_to root_path
            return  
          end
        else
          redirect_to root_path
          return  
        end
      else
        redirect_to root_path
        return
      end
    else
      redirect_to root_path
      return
    end
  end
  
  def getCategoryName(catName)
    if catName == "ico"
      return "ICO"
    elsif catName == "blockchain"
      return "BLOCKCHAIN"
    elsif catName == "fintech"
      return "FINTECH"
    elsif catName == "cryptocurrency"
      return "CRYPTOCURRENCY"
    elsif catName == "bieu-do-coin"
      return "BIỂU ĐỒ COIN"
    elsif catName == "chung-khoan"
      return "CHỨNG KHOÁN"
    elsif catName == "cong-nghe"
      return "CÔNG NGHỆ"
    elsif catName == "kinh-doanh"
      return "KINH DOANH"
    else
      redirect_to root_path
      return
    end
  end
end
