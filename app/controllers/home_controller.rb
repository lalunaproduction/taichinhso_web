class HomeController < ApplicationController
  def index
    @firstPost = Post.order("created_at DESC").offset(0).first
    @secondPosts = Post.order("created_at DESC").offset(1).first(3)
    @listPost = Post.order("created_at DESC").offset(4).all
    @firstCoin = Coin.order("created_at DESC").offset(0).first
  end
end
