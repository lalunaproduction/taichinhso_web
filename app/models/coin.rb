class Coin < ApplicationRecord
    include FriendlyId
    
    has_attached_file :image
    friendly_id :name, use:[:slugged]
    
    validates_attachment :image,
        content_type: { content_type: /\Aimage\/.*\z/ },
        size: { less_than: 2.megabyte }
        
        
    def normalize_friendly_id(input)
        input.to_slug.normalize(transliterations: :vietnamese).to_s
    end
end
