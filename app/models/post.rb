class Post < ApplicationRecord
    include FriendlyId
    
    has_attached_file :image
    friendly_id :title, use:[:slugged]
    belongs_to :category
    
    validates_attachment :image,
        content_type: { content_type: /\Aimage\/.*\z/ },
        size: { less_than: 5.megabyte }
        
        
    def normalize_friendly_id(input)
        input.to_slug.normalize(transliterations: :vietnamese).to_s
    end
end
