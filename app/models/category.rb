class Category < ApplicationRecord
    include FriendlyId
    
    has_many :posts
    friendly_id :name, use:[:slugged]
    
    def normalize_friendly_id(input)
        input.to_slug.normalize(transliterations: :vietnamese).to_s
    end
end
