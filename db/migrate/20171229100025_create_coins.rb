class CreateCoins < ActiveRecord::Migration[5.0]
  def change
    create_table :coins do |t|
      t.string :name
      t.attachment :image
      t.datetime :ico_from
      t.datetime :ico_to
      t.string :industry
      t.string :website
      t.string :created_by
      t.string :updated_by
      t.boolean :delete_flag

      t.timestamps
    end
  end
end
