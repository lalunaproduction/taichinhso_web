class CreatePosts < ActiveRecord::Migration[5.0]
  def change
    create_table :posts do |t|
      t.string :title
      t.string :description
      t.string :content
      t.attachment :image
      t.integer :category_id
      t.string :created_by
      t.string :updated_by
      t.boolean :delete_flag
      t.integer :view

      t.timestamps
    end
  end
end
